var express = require('express');
var router = express.Router();
const authMiddleware = require("../middleware/auth");
const scoresData = require ("../public/data/scoresData.json");

router.use(authMiddleware)

/* GET home page. */
router.get('/', function (req, res, next) {
    const scoresList = scoresData.scoresList;
    res.render('scores', {scoresList});
});

module.exports = router;
