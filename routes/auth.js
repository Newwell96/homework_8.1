var express = require('express');
const authorize = require("../modules/auth_checker");
// const isAuthorized = require("../modules/isAuthorized");
var router = express.Router();


/* GET users login. */
router.get('/', function (req, res, next) {
    res.render('auth', {hasError: req.query.error});
});

router.post('/', async function (req, res, next) {
    const authData = req.body;
    const login = authData.login;
    const password = authData.password;

    const sessionID = await authorize(login, password);
    if (sessionID === '') {
        res.redirect("/auth?error=true")
        return
    }

    res.cookie("sessionID", sessionID)
    res.redirect("/")
});

module.exports = router;
